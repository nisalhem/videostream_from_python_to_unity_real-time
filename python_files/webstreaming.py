import io
from imutils.video import VideoStream
from flask import Response, send_file
from flask import Flask
from flask import render_template
import threading
import imutils
import time
import cv2

# initialize the output frame and a lock used to ensure thread-safe
# exchanges of the output frames (useful for multiple browsers/tabs
# are viewing tthe stream)
outputFrame = None
lock = threading.Lock()

# initialize a flask object
app = Flask(__name__)

# initialize the video stream.
vs = VideoStream(src=0).start()
# Rhe camera sensor is allowed to warmup via sleep
time.sleep(2.0)

@app.route("/")
def index():
	# return the rendered template
	return render_template("index.html")

def capture_video_from_camera():
	# grab global references to the video stream, output frame, and
	# lock variables
	global vs, outputFrame, lock

	# loop over frames from the video stream
	while True:
		# read the next frame from the video stream, resize it,
		# convert the frame to grayscale, and blur it
		frame = vs.read()
		frame = imutils.resize(frame, width=400)

		# acquire the lock, set the output frame, and release the
		# lock
		with lock:
			outputFrame = frame.copy()
		
def generate():
	# grab global references to the output frame and lock variables
	global outputFrame, lock

	# loop over frames from the output stream
	while True:
		# wait until the lock is acquired
		with lock:
			# check if the output frame is available, otherwise skip
			# the iteration of the loop
			if outputFrame is None:
				continue

			# encode the frame in JPEG format
			(flag, encodedImage) = cv2.imencode(".jpg", outputFrame)

			# ensure the frame was successfully encoded
			if not flag:
				continue

		# return the byte array of the encoded image using mimetype jpg
		return send_file(io.BytesIO(encodedImage), mimetype='image/jpg')
		break

@app.route("/video_feed")
def video_feed():
	# call generate function whenever a request is received to the webpage
	return generate()

# check to see if this is the main thread of execution
if __name__ == '__main__':
	# start a thread that performs video capture from the camera
	t = threading.Thread(target=capture_video_from_camera)
	t.daemon = True
	t.start()

	# start the flask app on localhost, port 8000.
	# i.e. web server is host under the url: localhost:8000
	app.run(host="0.0.0.0", port="8000", debug=True,
		threaded=True, use_reloader=False)

# release the video stream pointer
vs.stop()