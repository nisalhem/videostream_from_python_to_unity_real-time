## Setup
1. The video captured by the mesh network is received into Windows environment 
by the python codes
2. The videostream thus received are transferred to Unity in the Windows environment.
 via a webserver.

## Execution steps
Run the webstreaming.py followed by the unity scene.

## Idea 
Python hosts web server while at the same time using another thread captures video 
from the available camera. The camera input frames thus captured  converted to jpg
format and streamed to the to the web server. Unity reads the image as a coroutine 
from the URL of the image.

References:
[https://blog.miguelgrinberg.com/post/video-streaming-with-flask](https://blog.miguelgrinberg.com/post/video-streaming-with-flask)
[https://www.pyimagesearch.com/2019/09/02/opencv-stream-video-to-web-browser-html-page/](https://www.pyimagesearch.com/2019/09/02/opencv-stream-video-to-web-browser-html-page/)
