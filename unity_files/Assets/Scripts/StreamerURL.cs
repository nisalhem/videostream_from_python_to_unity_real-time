﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class StreamerURL : MonoBehaviour
{
    public string url = "http://localhost:8000/video_feed";
    public RawImage frame;
    private Texture2D texture;

    // Use this for initialization
    void Start()
    {
        texture = new Texture2D(2, 2);
        //StartCoroutine(GetImageFromURL());
    }

    void Update()
    {
        StartCoroutine(GetImageFromURL());
    }

    // Send a web request to the given URL, download the jpg image, and
    // assign it to the texture of the RawImage
    IEnumerator GetImageFromURL()
    {
        yield return new WaitForEndOfFrame();
        using (WWW www = new WWW(url))
        {
            yield return www;
            www.LoadImageIntoTexture(texture);
            frame.texture = texture;
            //Start();
        }
    }
}



